#include <Ethernet.h>           //library for ethernet functions
#include <SPI.h>
#include <SD.h>
#include <Client.h>
#include <SD.h> 	
#include <StackArray.h>
#include "DHT.h"
#include "Classes.h"
#include <MemoryFree.h>


WebServer *server; // Сервер
Config *config; // Основной конфиг
Controller* controller; // Плата
Param* token; // Авторизационный токен

void setup(void)
{
  // Инициализация вывода и SD карты
  Controller::preInit();

  // Анализ файла конфигурации
  config = Config::createFromFile("config.txt");  
  // Если не найден mac то перезагружаем
  if (!config->check("controller_mac")
    || !config->check("controller_ip")
    || !config->check("controller_dns")
    || !config->check("controller_gateway")) {
    Controller::error("Ethernet params not found");
  }

  // Регистрация контроллера в сети
  Controller::init(
    config->get("controller_mac"), 
    config->get("controller_ip"), 
    config->get("controller_dns"), 
    config->get("controller_gateway")
  );

  // Объект для работы с сервером
  if (!config->check("server_ip") || !config->check("server_port")) {
    Controller::error("Server params not found");
  }
  server = new WebServer(config->get("server_ip"), config->get("server_port"));
  
  // Получение авторизационного токена
  StackArray<Param *> authorize;
  authorize.push(new Param("ip", config->get("controller_ip")));
  authorize.push(new Param("mac", config->get("controller_mac")));
  
  config->mergeWithStream(server, "/arduino/authorize", &authorize, CONFIG_DUPLICATE_REPLACE);

  // Очистка памяти
  authorize.clear();
  
  // Сохранение авторизационного токена
  if (!config->check("token")) {
    Controller::error("Token not found");
  }
  token = config->get("token");
  
  // Получение списка пинов
  StackArray<Param *> auth;
  auth.push(new Param("token", token));
  Config *pins = Config::createFromStream(server, "/arduino/pins", &auth);
  // Очистка памяти
  auth.clear();
  
  // Создание объекта контроллера с информацией о пинах
  if (!pins || !pins->getConfig() || !pins->getConfig()->count()) {
    Controller::error("Pins config is empty");
  }
  controller = new Controller(pins);

  // Завершение иницилазции. Вывод информации об оставшейся памяти
  Controller::postInit();
  delay(1000);
}

void loop(void)
{
  Controller::led(true, 100);
  Serial.println("New iteration...");
  Serial.print("Free memory: ");
  Serial.println(freeMemory());

  // Получение всех сенсоров
  StackArray<AbstractSensor*>* sensors = controller->getSensors();

  // Получение вектора датчик=значение для отправляемых датчиков
  StackArray<Param*> data;
  for(int i = 0; i < sensors->count(); i++) {
    AbstractSensor* sensor = sensors->peek(i);
    // Если датчик отправляемый
    if (sensor->isSendable()) {
      // формируем строку pinНомер.ИмяТипаДатчика
      String name("pin");
      name += sensor->getPin();
      name += ".";
      name += sensor->getName();
      data.push(new Param(name, sensor->getValue()));
    }
  }
  
  // Отправка информации на сервер о состоянии датчиков и приём управляющих комманд 
  StackArray<Param *> params;
  params.push(new Param("token", token));
  Config* response = Config::createFromStream(server, "/arduino/status", &params, METHOD_POST, &data);

  // Очистка памяти
  data.clear();
  params.clear();
  if (response->check("reset")) {
    Controller::reset();
  }
  // Анализ управляющих сигналов и установка значений для управляющих датчиков
  for(int i = 0; i < sensors->count(); i++) {
    AbstractSensor* sensor = sensors->peek(i);
    // формируем строку pinНомер.ИмяТипаДатчика
    String pin("pin");
    pin += sensor->getPin();
    pin += ".";
    pin += sensor->getName();
    // Если датчик управляемый и пришёл управляющий сигнал
    if (sensor->isControllable() && response->get(pin) != NULL) {
      sensor->setValue(response->get(pin));
    }
  }
  // Очистка памяти
  delete response;

  Serial.print("Free memory: ");
  Serial.println(freeMemory());
  Serial.println();
  delay(500);
}

