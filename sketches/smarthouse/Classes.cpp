#include "Classes.h"

void(* resetFunc) (void) = 0;

/**
 * Создаёт конфиг из файла
 * @property Stirng name - имя файла
 * @return Config* - объект конфигурации
 */
Config* Config::createFromFile(String name) 
{
	Serial.print("Create config object from file ");
	Serial.print(name);
	Serial.println("...");
	File config_file = SD.open(name.c_str());
	// Если файл успешно открывается
	if (config_file) {
		// Создаём объект
		Config* config = new Config(config_file);
		// Закрываем дескриптор
		config_file.close();
		Serial.println("Creating done.");
		Serial.println();
		config->print();
		return config;
	} else {
		Serial.print("Error opening ");
		Serial.println(name);
		Serial.println();
	}
}

/**
 * Создаёт конфиг из результата запроса к серверу
 * @property WebServer* server - объект сервера
 * @property String query
 * @property StackArray<Param*>* params
 * @property byte method
 * @property StackArray<Param*>* data
 * @return Config*
 */
Config* Config::createFromStream(WebServer* server, String query, StackArray<Param*>* params, byte method, StackArray<Param*>* data) 
{
	Serial.print("Create config object from query ");
	Serial.print(query);
	Serial.println("...");
	// Если запрос вернёл результат
	if (server->query(query, params, method, data)) {
		String data;
		// Получаем результат запроса
		data = server->getResponseText();
		// Создаём объект конфигурации из результата запроса
		Config* config = new Config(data);
		Serial.println("Creating done.");
		Serial.println();
		config->print();
		return config;
	} else {
		Serial.print("Error sending request ");
		Serial.println(query);
		Serial.println();
		return NULL;
	}
}

/**
 *
 * @property String data
 * @property int seek
 */
Config::Config(String data, int seek) 
{
	// если задан отступ, делаем обрезку
	if (seek > 0) {
		data = data.substring(seek);
	}
	// обрабатываем текст
	_parse(data);
}

/**
 *
 * @property File &file
 * @property int seek
 */
Config::Config(File &file, int seek) 
{
	// устанавливаем позицию
	file.seek(seek);
	String data;
	// пока поток доступен, читаем посимвольно
	while (file.available()) {
      data.concat((char)file.read());
    }
    // обрабатываем накопленный текст
    _parse(data);
}

/**
 *
 * @property EthernetClietn stream
 */
Config::Config(EthernetClient stream) 
{
	String data;
	// пока поток доступен, читаем посимвольно
	while (stream.available()) {
      data.concat((char)stream.read());
    }
    // обрабатываем накопленный текст
    _parse(data);
}

/**
 *
 * @property String data
 * @property int seek
 */
Config::~Config() 
{
	// очищаем память из под массива параметров
	_config.clear();
}

/**
 *
 * @property String line
 * @return Param*
 */
Param* Config::_parseLine(String line) 
{
	// Ищем позицию символа "="
	int spacePos = line.indexOf("=");
	// Получаем имя параметра, как строка до символа "="
	String name = line.substring(0, spacePos);
	// Убираем лишнии пробелы
	name.trim();
	// Берём значение параметра как остаток строки после символа "="
	line = line.substring(spacePos+1);
	// Убираем лишнии пробелы
	line.trim();
	// Создаём объект и возвращаем
	return new Param(name, line);
}

/**
 *
 * @property String data
 * @property byte duplicate
 * @return StackArray<Param*>*
 */
StackArray<Param*>* Config::_parse(String data, byte duplicate) 
{
	int lastLineBreak = 0;
	// Пока есть строки
	while(data.indexOf('\n', lastLineBreak) > 0) {	
		// Позиция перехода на новую строку
		int newLineBreak = data.indexOf('\n', lastLineBreak);
		// Записываем строку с прошлого перехода по текущий
		String line = data.substring(lastLineBreak, newLineBreak);
		// Устанавливаем значение прошлого перехода как текущее
		lastLineBreak = newLineBreak+1;
		// Если в строке есть полезные символы
		if (line.indexOf('\n') != 0 && line.length() > 0) {
			// Парсим строку
			Param *param = _parseLine(line);
			// Если необходимо обработать дубликаты, то пытаемся найти объект в массиве
			if ((duplicate == CONFIG_DUPLICATE_IGNORE || duplicate == CONFIG_DUPLICATE_REPLACE)  && check(param->getName())) {
				// Если необходимо заменить параметр
				if (duplicate == CONFIG_DUPLICATE_REPLACE) {
					// Получаем параметр
					Param *item = get(param->getName());
					// Устанавливаем новое значение
					item->setValue(param->getValue());
				}
				continue;
			}
			// Добавляем параметр в массив
			_config.push(param);
		}
	} ;
	// Берём последнюю строчку, если не было перевода каретки
	String line = data.substring(lastLineBreak);
	// Если строка содержит полезные символы
	if (line.indexOf('\n') != 0 && line.length() > 0) {
		// Парсим строку
		Param *param = _parseLine(line);
		// Если не нужно проверять дубликаты или объект не найден
		if (duplicate == CONFIG_DUPLICATE_ADD || !check(param->getName())) {
			// Доабвляет параметр в массив
			_config.push(param);
		// Если необходимо заменить
		} else if (duplicate == CONFIG_DUPLICATE_REPLACE) {
			Param *item = get(param->getName());
			// Устанавливаем новое значение
			item->setValue(param->getValue());
		}
	}
	return &_config;
}

/**
 *
 * @return String
 */
String Config::print()
{
	String data;
	Serial.println("Printing config object...");
	// Печатаем каждый элемент массива
	for(int i=0; i<_config.count(); i++) {
  		data.concat(_config.peek(i)->getName());
  		data.concat("(");
  		data.concat(_config.peek(i)->getType());
  		data.concat(")");
  		data.concat("=");
  		data.concat(_config.peek(i)->print());
  		data.concat("\n");
  	}
  	Serial.println(data);
  	return data;
}

/**
 *
 * @property String name
 * @return Param*
 */
Param* Config::get(String name) 
{
	for(int i=0; i<_config.count(); i++) {
		// Если имя параметра равно заданному
		if (_config.peek(i)->getName() == name) {
			return _config.peek(i);
		}
	}
	Serial.print(name);
	Serial.println(" not found!");
}

/**
 *
 * @property String name
 * @return bool
 */
bool Config::check(String name) 
{
	for(int i=0; i<_config.count(); i++) {
		// Если имя параметра равно заданному
		if (_config.peek(i)->getName() == name) {
			return true;
		}
	}
	return false;
}

/**
 *
 * @return StackArray<Param*>*
 */
StackArray<Param*>* Config::getConfig() 
{
	return &_config;
}


/**
 *
 * @property String data
 * @property byte duplicate
 * @return void
 */
void Config::mergeWithFile(String name, byte duplicate) 
{
	Serial.print("Merging config object with file ");
	Serial.print(name);
	Serial.println("...");
	File file = SD.open(name.c_str());
	// Если файл прочитался нормально
	if (file) {
		String data;
		// Пока файл доступен
		while (file.available()) {
	      data.concat((char)file.read());
	    }
	    // Если строка содержит данные
	    if (data.length()>0) {
	    	// Парсим строку с методом работы с дубликатов
	    	_parse(data, duplicate);
		}
		Serial.println("Merging done.");
		Serial.println();
		// Печатаем текущий массив
		print();
	} else {
		Serial.print("Error opening ");
		Serial.println(name);
		Serial.println();
	}
}

/**
 *
 * @property WebServer* server
 * @property String query
 * @property StackArray<Param*>* params
 * @property byte duplicate
 * @property byte method
 * @property StackArray<Param*>* data
 * @return void
 */
void Config::mergeWithStream(WebServer* server, String query, StackArray<Param*>* params, byte duplicate, byte method, StackArray<Param*>* data) 
{
	Serial.print("Merging config object with query ");
	Serial.print(query);
	Serial.println("...");
	// Если запрос вернул данные
	if (server->query(query, params, method, data)) {
		String data;
		// Получаем данные
		data = server->getResponseText();
		// Если текст есть
		if (data.length()>0) {
			// Парсим текст
			_parse(data, duplicate);
		}
		Serial.println("Merging done.");
		Serial.println();
		// Печатаем массив
		print();
	} else {
		Serial.print("Error sending request ");
		Serial.println(query);
		Serial.println();
	}
}

/**
 *
 * @property String name
 * @property String query
 * @return void
 */
Param::Param(String name, String value) 
{
	_name = name;
	_value = value;
	// Ищем первую точку
	int firstDotPos = _value.indexOf(".");
	// Ищем последнюю точку
	int lastDotPos = _value.lastIndexOf(".");
	// Если первый символ цифра
	if (isDigit(_value[0])) { 
		// Если точек нет
		if (firstDotPos == -1) {
			// Тогда значение число
			_type = PARAM_INT;
		// Если точка есть и она одна
		} else if (firstDotPos >= 0 && lastDotPos == firstDotPos) {
			// Тогда значение десятичное число
			_type = PARAM_FLOAT;
		// Если точек больше одной
		} else {
			// Иначе это байты
			_type = PARAM_BYTES;
		}
	// Иначе это строка
	} else {
		_type = PARAM_STRING;
	}
}

/**
 *
 * @property String name
 * @property Param* value
 * @return void
 */
Param::Param(String name, Param* value) 
{
	_name = name;
	_value = value->getValue();
	_type = value->getType();
}

/**
 *
 * @return String
 */
String Param::getName() 
{
	return _name;
}

/**
 *
 * @property String name
 * @return void
 */
void Param::setName(String name) 
{
	_name = name;
}

/**
 *
 * @return String
 */
String Param::getValue() 
{
	return _value;
}

/**
 *
 * @property String value
 * @return void
 */
void Param::setValue(String value)
{
	_value = value;
}

/**
 *
 * @return byte
 */
byte Param::getType() 
{
	return _type;
}

/**
 *
 * @return int
 */
int Param::toInt() 
{
	if (_type == PARAM_INT || _type == PARAM_FLOAT) {
		return _value.toInt();
	} else {
		return 0;
	}
}

/**
 *
 * @return float
 */
float Param::toFloat() 
{
	if (_type == PARAM_INT || _type == PARAM_FLOAT) {
		return atof(_value.c_str());
	} else {
		return 0;
	}
}

/**
 *
 * @return char*
 */
char* Param::toChars() 
{
	char* res = new char[_value.length()];
  	_value.toCharArray(res, _value.length());
	return res;
}

/**
 *
 * @return uint8_t*
 */
uint8_t* Param::toBytes() 
{
	if (_type == PARAM_BYTES) {
		int lastDotPos = 0;
		StackArray<byte> digits;
		// Пока найдены точки
		while (_value.indexOf(".", lastDotPos) > 0){
			// Берём позицию следующий точки
			int newDotPos = _value.indexOf(".", lastDotPos);
			// Добавляем в массив собранное число (только как байт)
			digits.push((_value.substring(lastDotPos, newDotPos).toInt() & 0x000000ff));
			lastDotPos = newDotPos+1;
		}
		// Добавляем в массив собранное число (только как байт)
		digits.push((_value.substring(lastDotPos).toInt() & 0x000000ff));
		// Формируем массив реальных байт
		uint8_t* bytes = new uint8_t[digits.count()];
		for(int i=0; i<digits.count(); i++) {
			bytes[i] = digits.peek(i);
		}
		return bytes;
	} else {
		return 0;
	}
}

/**
 *
 * @return String
 */
String Param::print() 
{
	if (_type == PARAM_INT) {
		return String(toInt());
	} else if (_type == PARAM_FLOAT) {
		return String(toFloat());
	} else if (_type == PARAM_BYTES) {
		String result;
		int lastDotPos = 0;
		StackArray<byte> digits;
		while (_value.indexOf(".", lastDotPos) > 0){
			// Берём позицию следующий точки
			int newDotPos = _value.indexOf(".", lastDotPos);
			// Добавляем в массив собранное число (только как байт)
			digits.push((_value.substring(lastDotPos, newDotPos).toInt() & 0x000000ff));
			lastDotPos = newDotPos+1;
		}
		// Добавляем в массив собранное число (только как байт)
		digits.push((_value.substring(lastDotPos).toInt() & 0x000000ff));
		// Формируем строку для вывода, при использовании байтов существовала проблема
		// нулевого байта
		for(int i=0; i<digits.count(); i++) {
			result.concat((int)digits.peek(i));
			result.concat(".");
		}
		// Печатаем без последней строчки
		result = result.substring(0, result.length()-1);
		return String(result);
	} else {
		return String(getValue());
	}
}

/**
 *
 * @property Param* ip
 * @property Param* port
 * @return void
 */
WebServer::WebServer(Param *ip, Param *port) 
{
	_ip = ip;
	_port = port;	
}

/**
 *
 * @return String
 */
String WebServer::getResponseText() 
{
	String data;
	// Пока подключение активно и есть данные
	while(_server.connected() && _server.available()) {
	      data.concat((char)_server.read());
	}
	// Ищем два перевода строки и обрезаем их, т.к это заголовки
	if (data.indexOf("\r\n\r\n") > -1) {
		data = data.substring(data.indexOf("\r\n\r\n")+4);
	}
	Serial.println("Data recieved:");
	Serial.println(data);
	Serial.println();
	// Остановить передачу данных
	_server.stop();
	return data;
}

/**
 *
 * @property String url
 * @property StackArray<Param*>* params
 * @property byte method
 * @property StackArray<Param*>* data
 * @property byte type
 * @return bool
 */
bool WebServer::query(String url, StackArray<Param*>* params, byte method, StackArray<Param*>* data, byte type) 
{
	_server.flush();
	_server.stop();
	delay(250);
	// Если адрес сервера ip а не текст
	if (_ip->getType() == PARAM_BYTES) {
		// создаём переменную для возможности очистить память
		uint8_t* bytes = _ip->toBytes();
		// производим подключение
		_server.connect(bytes, _port->toInt());
		// очищаем память
		delete bytes;
	} else {
		_server.connect(_ip->toChars(), _port->toInt());
	}
	// Ждём 5 секунд или пишем ошибку
	unsigned long time = millis();
	while(!_server.connected()) {
		unsigned long now = millis();
		if ((now - time) >= CONNECTION_TIMEOUT) {
			Serial.println("Connection timeout");
			return NULL;
		}
	}
	// Если подключение успешно
	if (_server.connected()) {
		// Формируем строку GET|POST /url?param=value&param=value HTTP/1.0
		String query("");
		if (method == METHOD_GET) {
			query += "GET";
		} else if (method == METHOD_POST) {
			query += "POST";
		}	
		query += " ";
		query += url;
		if (params->count() > 0) {
			query += "?";
			for(int i=0; i<params->count(); i++) {
				query += params->peek(i)->getName();
				query += "=";
				query += params->peek(i)->getValue();
				if (i < params->count()-1) {
					query += "&";
				}
			}
		}
		if (method == METHOD_POST) {
			query += " HTTP/1.0\r\n";
		} else {
			query += " HTTP/1.0\r\n";
		}
		// Если запрос отправляется POST то формируем заголовки с данными
		if (method == METHOD_POST && data->count() > 0) {
			String post;
			for(int i=0; i<data->count(); i++) {
				post += data->peek(i)->getName();
				post += "=";
				post += data->peek(i)->getValue();
				if (i < data->count()-1) {
					post += "&";
				}
			}
			query += "Content-Type: application/x-www-form-urlencoded\r\n";
			query += "Content-length: ";
			query += post.length();
			query += "\r\n";
			query += "\r\n";
			query += post;
		}
		// Отправляем запрос
		_server.println(query);
		Serial.println("Sending request:");
		Serial.println("------");
		Serial.println(query);
		Serial.println("------");
		Serial.println();
	}
	// Ждём 5 сек или пишем таймаут 
	time = millis();
	while(!_server.available()) {
		unsigned long now = millis();
		if ((now - time) >= CONNECTION_TIMEOUT) {
			Serial.println("Request timeout");
			return false;
		}
	}
	// Возвращаем что всё прошло успешно и данные получены
	return true;
}

/**
 *
 * @return void
 */
void Controller::preInit() 
{
	Serial.begin(115200);
	while (!Serial) {
		;
	}
	Serial.print("Free memory: ");
	Serial.println(freeMemory());
	Serial.println(); 

	Serial.println("Initializing SD card...");
	// On the Ethernet Shield, CS is pin 4. It's set as an output by default.
	// Note that even if it's not used as the CS pin, the hardware SS pin 
	// (10 on most Arduino boards, 53 on the Mega) must be left as an output 
	// or the SD library functions will not work. 
	pinMode(53,OUTPUT); // pin 53 must be left as an OUTPUT for the SD library to work
	pinMode(10,OUTPUT); // pin 10 must be left as an OUTPUT so it can be set HIGH
	digitalWrite(10,HIGH); // pin 10 needs to be put HIGH to explicitly deselect the WiFi HDG104 chip

	pinMode(SERVICE_LED,OUTPUT);

	while (!SD.begin(4)) {
		;
	}
	Serial.println("Initialization done.");
	Serial.println();
} 

/**
 *
 * @property Param* mac
 * @property Param* ip
 * @property Param* dns
 * @property Param* gateway
 * @return void
 */
void Controller::init(Param *mac, Param *ip, Param *dns, Param *gateway) 
{
	// Создаём перенные для возможности очистить память
	byte* mac_b = mac->toBytes();
	byte* ip_b = ip->toBytes();
	byte* dns_b = dns->toBytes();
	byte* gateway_b = gateway->toBytes();

	Serial.println("Initializing Ethernet...");
	// Производим подключение
	Ethernet.begin(mac_b, ip_b, dns_b, gateway_b);
	// Очищаем память
	delete mac_b, ip_b, dns_b, gateway_b;
	Serial.println("Initialization done.");
	Serial.println();
}

/**
 *
 * @return void
 */
void Controller::postInit() 
{
	Serial.print("Free memory: ");
	Serial.println(freeMemory());
	Serial.println("Setup complete.");
	Serial.println();
}

/**
 *
 * @property String message
 * @property int time
 * @property int sleep
 * @property bool reset
 * @property byte led
 * @return void
 */
void Controller::error(String message, int time, int sleep, bool reset, byte led)
{
	Serial.print("Error: ");
	Serial.println(message);
	unsigned long now = millis();
	while((millis() - now) < time) {
		delay(sleep);
		digitalWrite(led, HIGH);
		delay(sleep);
		digitalWrite(led, LOW);
	}
	if (reset) {
		resetFunc();
	}
}

/**
 *
 * @property bool on
 * @property byte led
 * @return void
 */
void Controller::led(bool on, int sleep, byte led)
{
	if (on) {
		digitalWrite(led, HIGH);
		if (sleep > 0) {
			delay(sleep);
			digitalWrite(led, LOW);
		}
	} else {
		digitalWrite(led, LOW);
		if (sleep > 0) {
			delay(sleep);
			digitalWrite(led, HIGH);
		}
	}
}

/**
 *
 */
void Controller::reset()
{
	resetFunc();
}

/**
 *
 * @property Config* pins
 * @return void
 */
Controller::Controller(Config *pins) 
{
	_pins = pins;
	// Оплучаем конфиг параметров
	StackArray<Param *>* pinConfig = _pins->getConfig();
	for(int i=0; i<pinConfig->count(); i++) {
		// Если в параметре нет точки а только имя пина
		if (pinConfig->peek(i)->getName().indexOf(".") == -1) {
			// Получаем целочисленное имя имя
			int pin = atoi(pinConfig->peek(i)->getName().substring(3).c_str());
			AbstractSensor *sensor;
			// Получаем тип датчика
			String pinType = pinConfig->peek(i)->getValue();
			// Если датчик температуры
			if (pinType == "temperature") {
				String sensorType("");
				sensorType += pinConfig->peek(i)->getName();
				sensorType += ".type";
				int sensorTypeByte = 11;
				if (pins->check(sensorType)) {
					sensorTypeByte = pins->get(sensorType)->toInt();
				}
				sensor = new TemperatureSensor(pinType, pin, (byte)(sensorTypeByte & 0x000000ff));
			// Если датчик диод
			} else if (pinType == "diode") {
				sensor = new DiodeSensor(pinType, pin);
			}
			// Добавляем в массив сенсеров
			_sensors.push(sensor);
		}
	}
	delete pinConfig;
}

/**
 *
 * @property uint8_t pin
 * @return AbstractSensor*
 */
AbstractSensor* Controller::getSensor(uint8_t pin) 
{
	for(int i=0; i<_sensors.count(); i++) {
		// Если пин равен заданному
		if (_sensors.peek(i)->getPin() == pin) {
			return _sensors.peek(i);
		}
	}
}

/**
 *
 * @return StackArray<AbstractSensor*>*
 */
StackArray<AbstractSensor*>* Controller::getSensors()
{
	return &_sensors;
}

/**
 *
 * @return String
 */
String AbstractSensor::getName() 
{
	return _name;
}

/**
 *
 * @return void
 */
uint8_t AbstractSensor::getPin() 
{
	return _pin;
}

/**
 *
 * @property String name
 * @property uint8_t pin
 * @property uint8_t type
 * @return void
 */
TemperatureSensor::TemperatureSensor(String name, uint8_t pin, uint8_t type) 
{
  _name = name;
  _pin = pin;
  _type = type;
  _dht = new DHT(_pin, _type);
  _dht->begin();
}

/**
 *
 * @return String
 */
String TemperatureSensor::getValue() 
{
  return String(_dht->readTemperature());
}

/**
 *
 * @property Param* value
 * @return void
 */
void TemperatureSensor::setValue(Param *value) 
{
 	
}

/**
 *
 * @return bool
 */
bool TemperatureSensor::isSendable() 
{
  return true;
}

/**
 *
 * @return bool
 */
bool TemperatureSensor::isControllable() 
{
  return false;
}

/**
 *
 * @property String name
 * @property uint8_t pin
 * @return void
 */
DiodeSensor::DiodeSensor(String name, uint8_t pin) 
{
  _name = name;
  _pin = pin;
}

/**
 *
 * @return String
 */
String DiodeSensor::getValue() 
{
  return String(digitalRead(_pin));
}

/**
 *
 * @property Param* value
 * @return void
 */
void DiodeSensor::setValue(Param *value) 
{
	if (value->toInt() == 1) {
		digitalWrite(_pin, HIGH);
	} else {
		digitalWrite(_pin, LOW);
	}
 	_value = value;
}

/**
 *
 * @return bool
 */
bool DiodeSensor::isSendable() 
{
  return false;
}

/**
 *
 * @return bool
 */
bool DiodeSensor::isControllable() 
{
  return true;
}