#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

#include "DHT.h" // Датчик температуры и влажности
#include "SD.h" // SD карта
#include "StackArray.h" // Массив-вектор
#include <Ethernet.h> // Сетевое подключение
#include <MemoryFree.h> // Свободная память

#define PARAM_STRING 1 // Значение является строкой
#define PARAM_INT 2 // Значение является числом
#define PARAM_FLOAT 4 // Значение является десятичным числом
#define PARAM_BYTES 8 // Число является набором байт

#define CONFIG_DUPLICATE_IGNORE 1 // Игнорировать дубликаты при добавлении новых элементов
#define CONFIG_DUPLICATE_REPLACE 2 // Заменять элемент новым при нахождении соответствия
#define CONFIG_DUPLICATE_ADD 4 // Добавлять элемент в конец массива

#define CONNECTION_TIMEOUT 5000 // Таймаут соединения с сервером и ожидания ответа
#define QUERY_TYPE_PLAIN 1 // Тип plain/text для ответа
#define METHOD_GET 1 // Метод GET для запроса
#define METHOD_POST 2 // Метод POST для запроса

#define SERVICE_LED 2 // Диод для сервисных сообщений

// Класс параметра конфигурации
class Param {
  protected:
    // Имя параметра
    String _name;
    // Значение
    String _value;
    // Тип: константы PARAM_*
    byte _type;
  public:
    // возвращает тип параметра
    byte getType();
    // возвращает имя параметра
    String getName();
    // устанавливает имя параметра
    void setName(String name);
    // печатает значение параметра с учётом типа
    String print();
    // конструктор принимающий имя и текстовое значение
    Param(String name, String value);
    // конструктор принимающий имя и параметр в качестве значения
    Param(String name, Param* param);
    // возвращает значение
    String getValue();
    // устанавливает значение
    void setValue(String value);
    // возвращает значение в виде байт
    uint8_t* toBytes();
    // возвращает значение в виде десятичного числа
    float toFloat();
    // возвращает значение в виде строки
    String toString();
    // возвращает значение в виде символов
    char* toChars();
    // возвращает значение в виде числа
    int toInt();
};

// Класс для работы с сервером
class WebServer {
    private:
        // IP и порт сервера
        Param *_ip,*_port;
        // объект для хранения объекта сервера
        EthernetClient _server;
    public:
        // конструктор принимающий параметры ip и порт
        WebServer(Param *ip, Param *port);
        // возвращает результат запроса с сервера
        String getResponseText();
        // отправляет запрос на сервер
        // url - url запроса до "?"
        // params - параметры после "?" в виде ключ=значение
        // method - метод отправки
        // data - параметры POST запроса
        // type - тип получаемых данных
        bool query(String url, StackArray<Param*>* params, byte method = METHOD_GET, StackArray<Param*>* data = NULL, byte type = QUERY_TYPE_PLAIN);
};

// Класс хранения конфигурации
class Config {
    private:
        // массив параметров
        StackArray<Param*> _config;
        // возвращает разобранную линию конфига в качестве параметра
        Param* _parseLine(String line);
        // возвращает список параметров
        // data - конфиг в виде строки
        // duplicate - метод обработки дубликатов: констакты CONFIG_DUPLICATE_*
        StackArray<Param*>* _parse(String data, byte duplicate = CONFIG_DUPLICATE_ADD);
    public:
        // создаёт конфиг из обработанного файла
        static Config* createFromFile(String name);
        // создает конфиг из запроса от сервера
        // server - объект сервера
        // url - url запроса до "?"
        // params - параметры после "?" в виде ключ=значение
        // method - метод отправки
        // data - параметры POST запроса
        static Config* createFromStream(WebServer* server, String query, StackArray<Param*>* params, byte method = METHOD_GET, StackArray<Param*>* data = NULL);
        // деструктор
        ~Config();
        // конструктор принимающий текстовый конфиг и позицию налача анализа
        Config(String data, int seek=0);
        // конструктор принимающий объект файла и позицию начала анализа
        Config(File &file, int seek=0);
        // конструктор принимающий объект клиента, подключённого к серверу
        Config(EthernetClient stream);
        // объединяет текущий конфиг с файлом используя метод обработки дубликатов
        void mergeWithFile(String name, byte duplicate = CONFIG_DUPLICATE_ADD);
        // объединяет текущий конфиг с запросом, используя метод обработки дубликатов
        // server - объект сервера
        // url - url запроса до "?"
        // params - параметры после "?" в виде ключ=значение
        // duplicate - метод обработки дубликатов: констакты CONFIG_DUPLICATE_*
        // method - метод отправки
        // data - параметры POST запроса
        void mergeWithStream(WebServer* server, String query, StackArray<Param*>* params, byte duplicate = CONFIG_DUPLICATE_ADD, byte method = METHOD_GET, StackArray<Param*>* data = NULL);
        // возвращает параметр по имени
        Param* get(String name);
        // проверяет наличие параметра по имени
        bool check(String name);
        // печатает конфиг
        String print();
        // возвращает массив параметров
        StackArray<Param*>* getConfig();
};

// Класс абстрактного сенсера
class AbstractSensor {
  protected:
    // имя типа датчика
    String _name;
    // пин датчика
    uint8_t _pin;
    // значение датника
    Param *_value;
  public:
    // возвращает значение датчика
    virtual String getValue(void) = 0;
    // является ли датчик отправляемым
    virtual bool isSendable(void) = 0;
    // является ли датчик управляемым
    virtual bool isControllable(void) = 0;
    // устанавливает значение параметра
    virtual void setValue(Param* value) = 0;
    // возвращает имя типа датчика
    String getName();
    // возвращает пин
    uint8_t getPin();
};

// Класс датчика температуры
class TemperatureSensor : public AbstractSensor { 
  private:
    // тип датчика температуры
    uint8_t _type;
    // класс работы с датчиком
    DHT *_dht;
  public:
    // конструктор принимающий имя типа, пин и тип датчика
    TemperatureSensor(String name, uint8_t pin, uint8_t type=11);
    // возвращает значение датчика
    String getValue(void);
    // устанавливает значение датчика
    void setValue(Param* value);
    // является ли датчик отправляемым
    bool isSendable(void);
    // является ли датчик управляемым
    bool isControllable(void);
};

// Класс диода
class DiodeSensor : public AbstractSensor { 
  public:
    // констуктор
    DiodeSensor(String name, uint8_t pin);
    // возвращает значение датчика
    String getValue(void);
    // устанавливает значение датчика
    void setValue(Param* value);
    // является ли датчик отправляемым
    bool isSendable(void);
    // является ли датчик управляемым
    bool isControllable(void);
};

// Класс контроллера
class Controller {
    private:
        // конфигурация пинов
        Config* _pins;
        // объекты сенсеров
        StackArray<AbstractSensor *> _sensors;
    public:
        // конструктор принимающий конфигруацию пинов
        Controller(Config* pins);
        // возвращает сенсор по номеру пина
        AbstractSensor * getSensor(uint8_t pin);
        // выполняет начальные установки
        static void preInit();
        // подключает плату к локальной сети
        static void init(Param *mac, Param *ip, Param *dns, Param *gateway);
        // выполяет окончательные установки
        static void postInit();
        // печать сообщения об ошибке и мигание сервисного диода
        static void error(String message, int time = 5000, int sleep = 100, bool reset = true, byte led = SERVICE_LED);
        // управление сервисным диодом
        static void led(bool on = true, int sleep = 0, byte led = SERVICE_LED);
        // перезагрузка платы
        static void reset();
        // возвращает массив сенсоров
        StackArray<AbstractSensor*>* getSensors();
};