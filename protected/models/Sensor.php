<?php

/**
 * This is the model class for table "sensor".
 *
 * The followings are the available columns in table 'sensor':
 * @property string $id
 * @property integer $pin
 * @property string $value
 * @property string $description
 * @property string $id_controller
 * @property string $id_room
 * @property string $id_sensor_type
 * @property string $params
 * @property string $last_active
 * @property integer $inactive
 */
class Sensor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sensor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pin', 'required'),
			array('pin, inactive', 'numerical', 'integerOnly'=>true),
			array('value, id_controller, id_room, id_sensor_type, last_active', 'length', 'max'=>11),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, pin, value, description, id_controller, id_room, id_sensor_type, params, last_active, inactive', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'logs' => array(self::HAS_MANY, 'Log', 'id_sensor'),
			'controller' => array(self::BELONGS_TO, 'Controller', 'id_controller'),
			'room' => array(self::BELONGS_TO, 'Room', 'id_room'),
			'type' => array(self::BELONGS_TO, 'SensorType', 'id_sensor_type'),
			'events' => array(self::HAS_MANY, 'SensorEvent', 'id_sensor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pin' => 'Pin',
			'value' => 'Value',
			'description' => 'Description',
			'id_controller' => 'Id Controller',
			'id_room' => 'Id Room',
			'id_sensor_type' => 'Id Sensor Type',
			'params' => 'Params',
			'last_active' => 'Last Active',
			'inactive' => 'Inactive',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('pin',$this->pin);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('id_controller',$this->id_controller,true);
		$criteria->compare('id_room',$this->id_room,true);
		$criteria->compare('id_sensor_type',$this->id_sensor_type,true);
		$criteria->compare('params',$this->params,true);
		$criteria->compare('last_active',$this->last_active,true);
		$criteria->compare('inactive',$this->inactive);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sensor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
