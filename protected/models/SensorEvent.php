<?php

/**
 * This is the model class for table "sensor_event".
 *
 * The followings are the available columns in table 'sensor_event':
 * @property string $id
 * @property string $id_sensor
 * @property string $id_event
 * @property integer $event_min
 * @property integer $event_max
 * @property string $id_device
 * @property string $id_action
 * @property integer $value
 * @property integer $last
 * @property integer $time_start
 * @property integer $time_end
 */
class SensorEvent extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sensor_event';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('event_min, event_max, value, last, time_start, time_end', 'numerical', 'integerOnly'=>true),
			array('id_sensor, id_event, id_device, id_action', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_sensor, id_event, event_min, event_max, id_device, id_action, value, last, time_start, time_end', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'sensor' => array(self::BELONGS_TO, 'Sensor', 'id_sensor'),
			'event' => array(self::BELONGS_TO, 'Event', 'id_event'),
            'device' => array(self::BELONGS_TO, 'Sensor', 'id_device'),
			'action' => array(self::BELONGS_TO, 'Event', 'id_action'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_sensor' => 'Id Sensor',
			'id_event' => 'Id Event',
			'event_min' => 'Event Min',
			'event_max' => 'Event Max',
			'id_device' => 'Id Device',
			'id_action' => 'Id Action',
			'value' => 'Value',
			'last' => 'Last',
			'time_start' => 'Time Start',
			'time_end' => 'Time End',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('id_sensor',$this->id_sensor,true);
		$criteria->compare('id_event',$this->id_event,true);
		$criteria->compare('event_min',$this->event_min);
		$criteria->compare('event_max',$this->event_max);
		$criteria->compare('id_device',$this->id_device,true);
		$criteria->compare('id_action',$this->id_action,true);
		$criteria->compare('value',$this->value);
		$criteria->compare('last',$this->last);
		$criteria->compare('time_start',$this->time_start);
		$criteria->compare('time_end',$this->time_end);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SensorEvent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
