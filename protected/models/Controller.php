<?php

/**
 * This is the model class for table "controller".
 *
 * The followings are the available columns in table 'controller':
 * @property string $id
 * @property string $name
 * @property string $id_room
 * @property string $description
 * @property integer $value
 * @property double $status
 * @property string $ip
 * @property string $token
 * @property string $mac
 * @property string $last_active
 */
class Controller extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'controller';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('token, mac', 'required'),
			array('value', 'numerical', 'integerOnly'=>true),
			array('status', 'numerical'),
			array('name', 'length', 'max'=>255),
			array('id_room, last_active', 'length', 'max'=>11),
			array('ip', 'length', 'max'=>15),
			array('token', 'length', 'max'=>32),
			array('mac', 'length', 'max'=>23),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, id_room, description, value, status, ip, token, mac, last_active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'room' => array(self::BELONGS_TO, 'Room', 'id_room'),
			'sensors' => array(self::HAS_MANY, 'Sensor', 'id_controller'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'id_room' => 'Id Room',
			'description' => 'Description',
			'value' => 'Value',
			'status' => 'Status',
			'ip' => 'Ip',
			'token' => 'Token',
			'mac' => 'Mac',
			'last_active' => 'Last Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('id_room',$this->id_room,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('value',$this->value);
		$criteria->compare('status',$this->status);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('mac',$this->mac,true);
		$criteria->compare('last_active',$this->last_active,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Controller the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
