<?php

Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');

return array(
    'theme'=>'bootsrap',
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Умный дом',
    'preload'=>array('log'),
    'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.vendor.*'
	),
    'modules'=>array(
            'admin',
			'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123',
            'ipFilters'=>array(), 
		),
	),
	'components'=>array(
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=test10',
			'emulatePrepare' => true,
			'username' => 'ave',
			'password' => 'ave',
			'charset' => 'utf8',
		),
		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
            'urlSuffix'=>'.html',
			'rules'=>array(
				'<controller:\w\d+>/<action:\w\d+>'=>'<controller>/<action>'
			),
		),
        'viewRenderer'=>array(
          'class'=>'application.extensions.smarty.ESmartyViewRenderer',
            'fileExtension' => '.tpl',
            //'pluginsDir' => 'application.smartyPlugins',
            //'configDir' => 'application.smartyConfig',
            //'prefilters' => array(array('MyClass','filterMethod')),
            //'postfilters' => array(),
            'config'=>array(
                'force_compile' => YII_DEBUG
            )
        ),
        'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
        'bootstrap'=>array(
            'class'=>'bootstrap.components.Bootstrap',
        ),
	),
	'params'=>require(dirname(__FILE__).'/params.php'),
);