<?php
class MobileController extends CController
{
    public function actionLogin() 
    {
        if (!$_POST['email'] || !$_POST['password']) {
            $this->render('status', array('param'=>json_encode(array('success' => false, 'code'=>'EMPTY_INPUT'))));
            return;
        }
        
        $user = User::model()->findByAttributes(
        array ('email'=>$_POST['email']),   
        array ('params'=>array(':password'=>$_POST['password']), 'condition' => '(md5(CONCAT(md5(:password),salt))) = password'));
        if ($user == 0) {
            $this->render('status', array('param'=>json_encode(array('success' => false, 'code'=>'USER_NOT_FOUND'))));
            return;
        }
        
        $session = new Session();
        $session->created = time();
        $session->last_active = time();
        $uin = md5(mt_rand(0,1000000000));
        $session->uin = $uin;
        $session->id_user = $user->id;
        $session->ip = getenv("REMOTE_ADDR");
        $session->agent = getenv("HTTP_USER_AGENT");
        $session->save();
        $this->render('status', array('param'=>json_encode(array('success' => true, 'result' => $uin))));   
    }
    
    public function actionAuthorize()
    {
        if (!$_POST['uin']) {
            $this->render('status', array('param'=>json_encode(array('success' => false, 'code'=>'EMPTY_UIN'))));
            return;
        }
        $session = Session::model()->findByAttributes(
            array('uin'=>$_POST['uin']), 
            array(
                'params'=>array(":now"=>time()),
                'condition'=>"last_active + 1209600 >= :now"
            ));
            
        if ($session == 0) {
           $this->render('status', array('param'=>json_encode(array('success' => false, 'code'=>'NOT_AUTHORIZED'))));
           return;
        }
        
        $session->last_active = time();
        $session->save();
        
        $this->render('status', array('param'=>json_encode(array('success' => true))));           
    }
    
    public function actionSensors()
    {
        if (!$_POST['uin']) {
            $this->render('status', array('param'=>json_encode(array('success' => false, 'code'=>'EMPTY_UIN'))));
            return;
        }
        
        $session = Session::model()->findByAttributes(
            array('uin'=>$_POST['uin']), 
            array(
                'params'=>array(":now"=>time()),
                'condition'=>"last_active + 1209600 >= :now"
            ));
            
        if ($session == 0) {
           $this->render('status', array('param'=>json_encode(array('success' => false, 'code'=>'NOT_AUTHORIZED'))));
           return;
        }
        
        $session->last_active = time();
        $session->save();
            
        $result = array();
        $sensors = Sensor::model()->with('type','controller','room')->findAllByAttributes(array(
            'inactive'=>0
        ),array(
            'order'=>'type.controllable DESC, room.order, type.order, inactive'
        ));
        foreach ($sensors as $sensor) {     
            $result[] = array('description' => $sensor->description, 
            'value' => $sensor->value, 
            'pin'=>$sensor->pin,
            'type'=> $sensor->type->name,
            'inactive'=> $sensor->inactive,
            'manual'=> $sensor->manual,
            'token'=> $sensor->controller->token,
            'controller'=> $sensor->controller->ip,
            'controllable'=> $sensor->type->controllable
            );   
        }
        
        $this->render('status', array('param'=>json_encode(array('success' => true, 'result' => $result))));  
    }
    
    public function actionStatus()
	{
	    if (!$_POST['uin']) {
            $this->render('status', array('param'=>json_encode(array('success' => false, 'code'=>'EMPTY_UIN'))));
            return;
        }
        
        $session = Session::model()->findByAttributes(
            array('uin'=>$_POST['uin']), 
            array(
                'params'=>array(":now"=>time()),
                'condition'=>"last_active + 1209600 >= :now"
            ));
            
        if ($session == 0) {
           $this->render('status', array('param'=>json_encode(array('success' => false, 'code'=>'NOT_AUTHORIZED'))));
           return;
        }
        
        $session->last_active = time();
        $session->save();
        
        $controller = Controller::model()->findByAttributes(array('token'=> $_POST['token']));
        if ($controller == 0) {
            $this->render('status', array('param'=>json_encode(array('success' => false, 'code'=>'TOKEN_TIMEOUT'))));
            return;
        }
        
        $time =  time();
        $result = array();
        $sensors = Sensor::model()->with('type')->findAllByAttributes(array('id_controller'=>$controller->id, 'inactive' => 0 ));
        foreach ($sensors as $sensor) {
            if ($sensor->type->controllable == 1 && isset($_POST["value_pin" . $sensor->pin . "_". $sensor->type->name])){
                $value = $_POST["value_pin" . $sensor->pin . "_". $sensor->type->name];
                $log = new Log();
                $log->date = time();
                $log->id_sensor = $sensor->id;
                $log->id_event = 1;
                $log->value = $value ;
                $log->save();
                $sensor->value = $value;
                $sensor->manual = 1;
                $sensor->save();
            }            
        }
        $this->render('status', array('param'=>json_encode(array('success' => true))));
	}
}
?>