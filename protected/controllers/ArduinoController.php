<?php
class ArduinoController extends CController
{
	public function actionStatus($token)
	{
        $controller = Controller::model()->findByAttributes(array('token'=> $token));
        if ($controller == 0) {
            return ;
        }
        $time =  time();
        $result = array();
        $changes = array();
        $sensors = Sensor::model()->with('type')->findAllByAttributes(array('id_controller'=>$controller->id, 'inactive' => 0 ));
        foreach ($sensors as $sensor) {
            if ($sensor->type->sendable == 1 && isset($_POST["value_pin" . $sensor->pin . "_". $sensor->type->name])){
                $value = $_POST["value_pin" . $sensor->pin . "_". $sensor->type->name];
                $log = new Log();
                $log->date = time();
                $log->id_sensor = $sensor->id;
                $log->id_event = 1;
                $log->value = $value ;
                $log->save();
                if ($sensor->value == $value) {
                    $changes["value_pin" . $sensor->pin . "_". $sensor->type->name] = false;    
                } else {
                    $changes["value_pin" . $sensor->pin . "_". $sensor->type->name] = true;
                }
                $sensor->value = $value;
                $sensor->save();
            } 
            $value = $sensor->value;
            $events = SensorEvent::model()->with("event", "action", "device", "device.type")->findAllByAttributes(
                array ('id_sensor'=>$sensor->id, 'inactive'=>0),   
                array ('params'=>array(':value'=>$value, ':time'=>$time), 'condition' => '(event_min is NULL or :value >= event_min)  
                and (event_max is NULL or :value < event_max) and 
                (time_start is NULL or :time >= time_start)  and 
                (time_end is NULL or :time < time_end)'));
            if (is_array($events) && !empty($events)) {
                foreach ($events as $event) {
                    $manual = false;
                    $manual_value = null;
                    if ($event->device->manual == 1) {
                        $manual = true;
                        $manual_value = $event->device->value;
                    }
                    $value = $event->value;
                    if ($value == '') {
                       $value = $event->action->value;
                        if ($value == '' && !$manual) {
                            $value = $event->device->value;
                            if ($value == '') {
                                $value = 0;
                            }
                        } elseif ($value == '') {
                            $value = 0;
                        }
                    } 
                    if ($manual && $value == $manual_value) {
                        $event->device->manual = 0;
                        $event->device->save();
                    } elseif (!$manual) {
                        $event->device->value = $value;
                        $event->device->save();
                    }
                   //$result[] = "pin" . $event->device->pin . "." . $event->device->type->name . "=". $value;
                   if ($event->last == 1) {
                        break(2);
                   }       
               }
           }
        }
        
        $sensors = Sensor::model()->with('type')->findAllByAttributes(array('id_controller'=>$controller->id, 'inactive' => 0 ));
        foreach ($sensors as $sensor) {
            if ($sensor->type->controllable == 1) {
                if ($sensor->params != "" && stripos($sensor->params, "trigger=") === 0) {
                    $trigger = trim(substr($sensor->params, strlen("trigger=")));
                    $pin = explode(".", $trigger);
                    $pin[0] = substr($pin[0], 3);
                    
                    if (isset($_POST["value_".str_replace(".", "_", $trigger)]) && $changes["value_".str_replace(".", "_", $trigger)]) {
                        if ($sensor->value == "1") {
                            $sensor->value = 0; 
                            $sensor->manual = 1;   
                        } else {
                            $sensor->value = 1;
                            $sensor->manual = 1;
                        }
                        $sensor->save();
                    }
                }
                $result[] = "pin" . $sensor->pin . "." . $sensor->type->name . "=". $sensor->value;
            }
        }
        
        $controller->last_active = time();
        $controller->save();
        $this->render('status', array('param'=>implode("\n",$result)));
	}
    
    public function actionAuthorize($ip, $mac) 
    {
        $model = Controller::model()->findByAttributes(array('ip' => $ip,
        'mac'=>$mac));
        if ($model != 0)
        {
           $token = md5(mt_rand(0,1000000));
           $model->token = $token;
           $model->save();
           $this->render('status', array('param'=>"token=".$token)); 
        }      
    }

    public function actionPins($token) 
    {
        $controller = Controller::model()->findByAttributes(array('token'=> $token));
        if ($controller == 0) {
            return ;
        }
        $sensors = Sensor::model()->with('type')->findAllByAttributes(array('id_controller'=>$controller->id, 'inactive' => 0 ),array(
            'order'=>'type.order'
        ));
        foreach ($sensors as $sensor) {     
            $result[] = "pin" . $sensor->pin . "=" . $sensor->type->name;   
            if ($sensor->params != "") {
                $params = explode("\n",trim($sensor->params));
                foreach ($params as $param){
                   $result[] = "pin" . $sensor->pin . ".". $sensor->type->name . "." . $param;  
                }
            } 
        }
        $this->render('status', array('param'=>implode("\n",$result)));
    }
}